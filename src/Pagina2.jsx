
import Titol from './Titol';


function Pagina2(props){

    return (<>
    <hr />
    <Titol titol={props.titol} color={props.color}/>
    <hr />
    </>)
  }

  
  export default Pagina2;
