import {useState} from 'react';

import Pantalla from './Pantalla';
import Botonera from './Botonera';

import './calculadora.css';



export const Calculadora = () => {

    const [mostrar, setMostrar] = useState(0);

    function pulsa(e){
        console.log("PULSA:"+e);
        setMostrar(mostrar+e);
        switch (e) {
            case 'C':
                setMostrar('');
                break;
        
            default:
                break;
        }
    }

    
    const botones = [
        '1','2','3','/', '4', '5', '6', '*', '7','8','9','-','C','0','=','+'
    ];

    return (
        <div className="calculadora">
            <Pantalla mostrar={mostrar}/>
            <Botonera botones={botones} pulsa={pulsa} />
        </div>
    )
}

export default Calculadora;