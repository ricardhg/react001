


function Boton(props){

    const cssboton = props.valor==='C' ? 'boton rojo' : 'boton';

    return <div className={cssboton} onClick={()=>props.onClick(props.valor)}>{props.valor}</div>
}



function Botonera(props){



    return (
        <div className="botonera">
            {props.botones.map((e,idx)=><Boton key={idx} valor={e} onClick={()=>props.pulsa(e)} />)}
        </div>
    )




}

export default Botonera;
