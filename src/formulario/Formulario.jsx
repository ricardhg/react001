import {useState} from 'react';

import {Form, Button} from 'react-bootstrap';
import './formulario.css';


function Formulario() {

    const [email, setEmail] = useState("ricard@xxxx.com");
    const [emailclass, setEmailclass] = useState("");

    function enviarFormulario(e){
        e.preventDefault();
        if(!email.includes("@")){
            setEmailclass("is-invalid");
        } else {
            setEmailclass("");
        }
    }

    function cambioEmail(e){
        let x = e.target.value;
        setEmail(x);
        if(!x.includes("@")){
            setEmailclass("is-invalid");
        } else {
            setEmailclass("");
        }
    }

    return (
        <>
        <Form onSubmit={enviarFormulario}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control className={emailclass} value={email} name="email" onInput={cambioEmail} type="text" placeholder="Enter email" />
              
            </Form.Group>
            <Button type="submit" >Enviar</Button>{' '}<Button type="button" variant="danger" onClick={()=>setEmail("")} >borrar</Button>
      
        </Form>
            
        </>

    )
}


export default Formulario;