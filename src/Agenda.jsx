import { Table } from "react-bootstrap";


//const Agenda = () => {




function convierte_a_tabla(contacto){
    return (
        <tr key={contacto.email}>
            <td>{contacto.nombre}</td>
            <td>{contacto.email}</td>
        </tr>
    )
}


function Agenda() {

    const mis_contactos = [
        { nombre: "pep", email: "pep@xxx"},
        { nombre: "lara", email: "lara@xxx"},
        { nombre: "aina", email: "aina@xxx"},
        { nombre: "bianca", email: "bbb@xxx"},
    ];

    let filas_de_la_tabla = mis_contactos.map(convierte_a_tabla);
    return (
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                {filas_de_la_tabla}
              
            </tbody>
        </Table>
    )
}


export default Agenda;