


function ListaCompra(props){

    // function string_a_li(s){
    //     return <li>{s}</li>;
    // }
    // let items = props.lista.map(string_a_li)
    //let items = props.lista.map(e => <li>{e}</li>);
   
    let items = props.lista.map((e,idx) => <li key={idx}>{e}</li>);


    return (
        <ul>
            {items}
        </ul>
    )

}


export default ListaCompra;