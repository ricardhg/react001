import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import Pagina1 from './Pagina1';
import Pagina2 from './Pagina2';
import Contador from './Contador';

import Calculadora from './calculadora/Calculadora';
import Bolera from './Bolera';
import Bingo from './Bingo';
import Agenda from './Agenda';
import Formulario from './formulario/Formulario';

import VespaVerda from './imagenes/vespa_verda.jpg';


import ListaCompra from './ListaCompra';



function App() {

  let listaNumeros = [4,7,2,5,9,1,3,7,7,7,75,8,0];

  let cosas = ["Fruta", "Cerveza", "Pasta", "Arroz"];

  return (<>

<Formulario />
<br />
<br />


  <Contador />
  <br />
  <br />
  <br />
  <br />
  <Agenda />
  <hr />
  <Bingo lista={listaNumeros} />
  <hr />
    <ListaCompra lista={cosas} />
    <Bolera numero={3} />
    <Pagina1 titol="MI BLOG DE MOTOS" color="verd" />
    <Pagina2 titol="VESPA FOREVER" color="blau" />
    <img src="/img/vespa.jpg" alt="vespa" width="300px" />
    <img src={VespaVerda} alt="vespa2" width="300px" />
  </>
  );
}

export default App;
