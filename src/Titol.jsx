

import { AiFillAmazonCircle } from "react-icons/ai";

function Titol(props) {

    return <h1 className={props.color}>{props.titol} <AiFillAmazonCircle /></h1>
  }
  
  
  export default Titol;