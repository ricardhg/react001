
import Titol from './Titol';

import './pagina1.css';

function Pagina1(props) {

    

    return (
        <div className="pagina1">
            <hr />
            <Titol titol={props.titol} color={props.color} />
            <hr />
        </div>)
}

export default Pagina1;