import { useState } from 'react';


function Contador(){

    const [cantidad, setCantidad] = useState(10);
    const [byte, setByte] = useState(0);
    const [activo, setActivo] = useState(false);
    

    let estiloBarra={
        backgroundColor: "rgb(0,"+byte+",0)"
    };

    let claseBola = "bolax verde";
    if (activo === true){
        claseBola = "bolax rojo";
    }

    // function cambiaBola(){
    //     setActivo(!activo);
    // }

    //const cambiaBola = () => setActivo(!activo);

    function clicar(){
        setCantidad(cantidad+1);
    }
    // function actualizaRango(e){
    //     //console.log(e);
    //     setByte(e.target.value);
    // }


    // const actualizaRango = (e) => setByte(e.target.value);

    return (
        <>
        <h1 >{cantidad}</h1>
        <button onClick={clicar} >Incrementar</button>
        <br />
        <input type="range" min="0" max="255" onChange={(e) => setByte(e.target.value)} />
        <h3>{byte}</h3>
        <div className="barra" style={estiloBarra}></div>
        <div className={claseBola} onClick={() => setActivo(!activo)}/>
        </>
    )
}

export default Contador;